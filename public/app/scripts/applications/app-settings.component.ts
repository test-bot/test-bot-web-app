import { Component } from '@angular/core'
import {RouteParams, Router} from '@angular/router-deprecated';
import {AppsDataService} from './apps-data-service';
import {ScenariosDataService} from '../scenarios/scenarios-data-service';
import {Application} from './application';
import {AccessTokensComponent} from './access-tokens.component';
import {SideMenuComponent} from '../common/side-menu/side-menu.component';
import {BreadcrumbState, AppendToBreadcrumbAction, RemoveFromBreadcrumbAction} from '../common/breadcrumb/breadcrumb';

@Component({
    templateUrl: 'app/templates/applications/app-settings.html',
    directives: [AccessTokensComponent, SideMenuComponent]
})
export class AppSettingsComponent {
    constructor(
        private dataService: AppsDataService,
        scenariosService: ScenariosDataService,
        params: RouteParams,
        private router: Router,
        private state: BreadcrumbState) {
            this.scenariosService = scenariosService;

            this.loading = true;
            dataService.getOne(params.get("appId"))
                .subscribe((app) => {
                    this.loading = false;
                    this.app = app;
                    this.lastTimeDataUpdated = this.app.lastTimeDataUpdated;
                });
    }

    ngOnInit() {
        var action = new AppendToBreadcrumbAction();
        action.displayName = 'Settings';
        action.routeName = 'Settings';
        action.params = {};
        action.router = this.router;
        this.state.publisher.next(action);
    }

    ngOnDestroy() {
        this.state.publisher.next(
            new RemoveFromBreadcrumbAction('Settings'));
    }

    updateSessionsData() {
        this.updatingData = true;
        this.scenariosService.updateSessionsData(this.app.id)
            .subscribe((response) => {
                this.updatingData = false;
                if(response.state === 0) {
                    this.dataStateInfo = 'The sessions data will be updated soon.';
                    this.lastTimeDataUpdated = new Date(response.lastTimeDataUpdated);
                } else if (response.state === 1) {
                    this.dataStateInfo = 'You will be able to update the data after '
                        + response.nextAvailableUpdateAfterSec + ' seconds.';
                }
            });
    }

    deleteApplication () {
        swal({
            title: "Delete application?",
            text: "All data will be deleted and cannot be restored.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true
        }, () => {
            this.dataService.delete(this.app)
                .subscribe(() => this.router.navigateByUrl('/'));
        });
    }

    private app: Application;
    private scenariosService: ScenariosDataService;
    private loading: boolean;
    private updatingData: boolean;
    private dataStateInfo: string;
    private lastTimeDataUpdated: Date;
}