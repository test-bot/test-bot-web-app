import {Component, OnInit} from '@angular/core'
import {ROUTER_DIRECTIVES, Router} from '@angular/router-deprecated'

import {AppsDataService} from './apps-data-service'
import {Application} from './application'

@Component({
    templateUrl: 'app/templates/applications/apps.html',
    directives: ROUTER_DIRECTIVES
})
export class AppsComponent implements OnInit {
    constructor(dataService: AppsDataService, private router: Router) {
        dataService.get().subscribe(
            (apps: Application[]) => {
                this.apps = apps;
            });
    }

    ngOnInit() {
        $('.navbar-minimalize').hide();
    }

    private apps: Application[]
}