import {Injectable, Inject} from '@angular/core';
import {Http, Headers, Response} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';

@Injectable()
export class TokensDataService {
    
    constructor(private http: Http) {
    }

    public generateToken() {
        return this.http.post('/token', '')
            .map(res => res.json());
    }
}