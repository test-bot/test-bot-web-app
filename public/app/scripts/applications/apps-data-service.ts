import {Injectable, Inject} from '@angular/core';
import {Http, Headers, Response} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';

import {Application} from './application';
import {AuthenticationService} from '../authentication-service';

@Injectable()
export class AppsDataService {
    constructor(private http: Http, @Inject('appConfig') private config, private authService: AuthenticationService) {
        this.appsEndpoint = config.apiServerUrl + '/apps/';
    }

    // Get request and serialize the result to JSON
    public get() {
        return this.http.get(this.appsEndpoint, {
            headers: this.authService.getAuthorizationHeader()
        })
            .map(res => res.json().items)
            .do(app => app.lastTimeDataUpdated = new Date(app.lastTimeDataUpdated));
    }

    public getOne(id: string | number) {
        return this.http.get(this.appsEndpoint + id, {
            headers: this.authService.getAuthorizationHeader()
        })
            .map(res => res.json())
            .do(app => {
                if (app.lastTimeDataUpdated) {
                    app.lastTimeDataUpdated = new Date(app.lastTimeDataUpdated);
                }
            });
    }

    // Post request with Headers
    public post(app : Application) {
        var headers = this.authService.getAuthorizationHeader();
        headers.append('Content-Type', 'application/json');

        return this.http.post(this.appsEndpoint,
            JSON.stringify(app),
            {
                headers: headers
            })
            .map(res => res.json());
    }

    public delete(app: Application) {
        var headers = this.authService.getAuthorizationHeader();
        headers.append('Content-Type', 'application/json');

        return this.http.delete(this.appsEndpoint + app.id, {
            headers: headers
        });
    }

    private appsEndpoint;
}