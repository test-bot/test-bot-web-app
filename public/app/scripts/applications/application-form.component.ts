import {Component, OnInit} from '@angular/core';
import {NgForm}    from '@angular/common';
import {Router}    from '@angular/router-deprecated'

import {Application} from './application';
import {AppsDataService} from './apps-data-service';

@Component({
    selector: 'application-form',
    templateUrl: '/app/templates/applications/application-form.html'
})
export class ApplicationFormComponent implements OnInit {
    constructor(public dataService: AppsDataService, private router: Router) {
    }

    model = new Application();

    upsert = () => {
        this.dataService.post(this.model)
            .subscribe((app) => {
                this.router.navigate(['AppDetails', { appId: app.id }]);
            });
    }

    ngOnInit() {
        $('.navbar-minimalize').hide();
    }
}