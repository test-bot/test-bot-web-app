import {ObjectState} from '../object-state';

export class Application {
    constructor(
        id?: string,
        name?: string,
        url?: string,
        state?: ObjectState,
        lastTimeDataUpdated?: Date) {}

    id: string;
    name: string;
    url: string;
    state: ObjectState;
    lastTimeDataUpdated: Date;
}