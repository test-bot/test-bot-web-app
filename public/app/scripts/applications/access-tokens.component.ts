import {Component, OnInit} from '@angular/core';
import {TokensDataService} from './tokens-data-service';

@Component({
    selector: 'access-tokens',
    templateUrl: 'app/templates/applications/access-tokens.html'
})
export class AccessTokensComponent implements OnInit  {
    constructor(private tokensService: TokensDataService) {
    }

    ngOnInit() {
    }

    generateToken() {
        this.tokensService.generateToken()
            .subscribe(r => {
                this.currentToken = r.accessToken;
                this.generated = true;
            });
    }

    public description: String;
    public currentToken: String;
    public generated: boolean;
}