import {Component} from '@angular/core'
import {ApplicationFormComponent} from './application-form.component'

@Component({
    templateUrl: 'app/templates/applications/create-app.html',
    directives: [ApplicationFormComponent]
})
export class CreateAppComponent {

}