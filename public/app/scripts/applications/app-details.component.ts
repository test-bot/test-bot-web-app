import {Component, OnInit, OnDestroy} from '@angular/core'
import {RouteParams, ROUTER_DIRECTIVES, RouteConfig, Router} from '@angular/router-deprecated';
import {AppsDataService} from './apps-data-service';
import {ScenariosDataService} from '../scenarios/scenarios-data-service';
import {Application} from './application';
import {SuitesComponent} from '../suites/suites.component';
import {TestResultsComponent} from '../test-results/test-results.component';
import {AppSettingsComponent} from './app-settings.component';
import {TestDataComponent} from '../test-data/test-data.component';
import {BreadcrumbState, BreadcrumbComponent, AppendToBreadcrumbAction, RemoveFromBreadcrumbAction, AppendToBreadcrumbDelayedAction}
    from '../common/breadcrumb/breadcrumb';
import {SideMenuComponent} from '../common/side-menu/side-menu.component';
import {Subject} from 'rxjs/Subject';

@Component({
    templateUrl: 'app/templates/applications/app-details.html',
    directives: [ROUTER_DIRECTIVES, BreadcrumbComponent, SideMenuComponent]
})
@RouteConfig([
    { path: '/suites/...', name: 'Suites', component: SuitesComponent, useAsDefault: true },
    { path: '/test-results/...', name: 'TestResults', component: TestResultsComponent },
    { path: '/test-data', name: 'TestData', component: TestDataComponent },
    { path: '/settings', name: 'Settings', component: AppSettingsComponent }

])
export class AppDetailsComponent implements OnInit, OnDestroy {
    constructor(private dataService: AppsDataService,
        private scenariosService: ScenariosDataService,
        private params: RouteParams,
        private router: Router,
        private state: BreadcrumbState) {
        this.scenariosService = scenariosService;
    }

    ngOnInit() {
        var bcSubject = new Subject<AppendToBreadcrumbAction>();
        this.state.publisher.next(new AppendToBreadcrumbDelayedAction(bcSubject));

        this.loading = true;
        this.dataService.getOne(this.params.get("appId"))
            .subscribe((app) => {
                this.loading = false;
                this.app = app;
                var action = new AppendToBreadcrumbAction();
                action.displayName = this.app.name;
                action.routeName = 'AppDetails';
                action.params = { appId: this.app.id };
                action.router = this.router;
                bcSubject.next(action);
            });
    }

    ngOnDestroy() {
        this.state.publisher.next(
            new RemoveFromBreadcrumbAction('AppDetails'));
    }

    private app: Application;
    private loading: boolean;
    private routeConfig;
}