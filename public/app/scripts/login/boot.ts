import {LoginAppComponent}     from './login-app.component';
import {bootstrap}        from '@angular/platform-browser-dynamic';
import {LocationStrategy, Location, HashLocationStrategy } from '@angular/common';
import {HTTP_PROVIDERS} from '@angular/http';
import {provide} from '@angular/core';

declare var appConfig: any;

bootstrap(LoginAppComponent, [
    HTTP_PROVIDERS,
    provide('appConfig', { useValue: appConfig }),
    provide(LocationStrategy, { useClass: HashLocationStrategy })
]);