import {Component} from '@angular/core';
import {Http} from '@angular/http';

@Component({
    selector: 'login-app',
    templateUrl: 'app/templates/login/login-app.component.html'
})
export class LoginAppComponent {
    constructor(private http: Http) {

    }

    linkedInLogin() {
        this.http.get('auth/linkedin').subscribe(r => r.url);
    }

    saveToken(token: String) {
        console.log("token", token);
    }
}