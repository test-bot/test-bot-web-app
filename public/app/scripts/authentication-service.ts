import {Headers} from '@angular/http';

export class AuthenticationService {
    constructor() {
    }

    getAuthorizationHeader() : Headers {
        var accessToken = localStorage.getItem('usabilitics_access_token');
        return new Headers({ Authorization: 'Bearer ' + accessToken });
    }
}