import {Component, OnInit, OnDestroy} from '@angular/core';
import {ROUTER_DIRECTIVES, Router, RouteParams} from '@angular/router-deprecated';

import {TestResults} from './test-results';
import {TestResultsDataService} from './test-results-data-service';
import {ScenarioLastResultsComponent} from './scenario-last-results.component';
import {AppendToBreadcrumbAction, RemoveFromBreadcrumbAction, BreadcrumbState} from '../common/breadcrumb/breadcrumb';

@Component({
    templateUrl: 'app/templates/test-results/test-results-details.html',
    directives: [ROUTER_DIRECTIVES, ScenarioLastResultsComponent]
})
export class TestResultsDetailsComponent implements OnInit {

    constructor(private state: BreadcrumbState, private routeParams: RouteParams, private router: Router, private testResultsService: TestResultsDataService) {
        // code...
    }

    ngOnInit() {
        var action = new AppendToBreadcrumbAction();
        action.displayName = 'Test results details';
        action.routeName = 'TestResultsDetails';
        action.params = { runId: this.routeParams.get('runId') };
        action.router = this.router;
        this.state.publisher.next(action);

        this.loading = true;
        this.appId = this.router.parent.parent.parent.currentInstruction.component.params['appId'];
        this.testResultsService.getOne(this.appId, this.routeParams.get('runId'))
            .subscribe(r => {
                this.testResults = r;
                this.loading = false;
            });
    }

    ngOnDestroy() {
        this.state.publisher.next(
            new RemoveFromBreadcrumbAction('TestResultsDetails'));
    }

    public testResults: TestResults;
    public loading;
    public appId: String;
}