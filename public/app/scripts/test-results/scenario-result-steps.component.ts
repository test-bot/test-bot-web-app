import {Component, OnInit} from '@angular/core';
import {ROUTER_DIRECTIVES, Router, RouteParams} from '@angular/router-deprecated';

import {ExecutedStep} from './executed-step';
import {TestResultsDataService} from './test-results-data-service';

@Component({
    templateUrl: 'app/templates/test-results/scenario-result-steps.html',
    directives: ROUTER_DIRECTIVES
})
export class ScenarioResultStepsComponent implements OnInit {

    constructor(private routeParams: RouteParams, private router: Router, private testResultsService: TestResultsDataService) {
        // code...
    }

    ngOnInit() {
        var appId = this.router.parent.parent.parent.parent.currentInstruction.component.params['appId'];
        var runId = this.router.parent.parent.currentInstruction.component.params['runId'];
        var scenarioId = this.router.parent.parent.currentInstruction.component.params['scenarioId'];

        this.loadingSteps = true;
        this.testResultsService.getExecutedSteps(appId, runId, scenarioId)
            .subscribe(s => {
                this.steps = s;
                this.loadingSteps = false;
            });
    }

    public steps: ExecutedStep[];
    public loadingSteps: boolean;
}