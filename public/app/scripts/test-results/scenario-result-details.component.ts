import {Component, OnInit, OnDestroy} from '@angular/core';
import {ROUTER_DIRECTIVES, Router, RouteParams, RouteConfig} from '@angular/router-deprecated';

import {TestResults} from './test-results';
import {TestScenarioResult} from './test-scenario-result';
import {ExecutedStep} from './executed-step';
import {TestResultsDataService} from './test-results-data-service';
import {ScenarioResultStepsComponent} from './scenario-result-steps.component';
import {ScenarioResultStepDetailsComponent} from './scenario-result-step-details.component';
import {ScenarioLastResultsComponent} from './scenario-last-results.component';
import {AppendToBreadcrumbAction, RemoveFromBreadcrumbAction, BreadcrumbState} from '../common/breadcrumb/breadcrumb';

@Component({
    templateUrl: 'app/templates/test-results/scenario-result-details.html',
    directives: [ROUTER_DIRECTIVES, ScenarioLastResultsComponent]
})
@RouteConfig([
    { path: '/steps', name: 'ScenarioResultSteps', component: ScenarioResultStepsComponent, useAsDefault: true },
    { path: '/steps/:stepId', name: 'ScenarioResultStepDetails', component: ScenarioResultStepDetailsComponent},
])
export class ScenarioResultDetailsComponent implements OnInit, OnDestroy {

    constructor(private state: BreadcrumbState, private routeParams: RouteParams, private router: Router, private testResultsService: TestResultsDataService) {
        // code...
    }

    ngOnInit() {
        this.appId = this.router.parent.parent.parent.currentInstruction.component.params['appId'];

        var action = new AppendToBreadcrumbAction();
        action.displayName = 'Scenario result details';
        action.routeName = 'ScenarioResultDetails';
        action.params = { runId: this.routeParams.get('runId') };
        action.router = this.router;
        this.state.publisher.next(action);

        this.loadingStats = true;
        this.testResultsService.getOneScenarioResult(this.appId, this.routeParams.get('runId'), this.routeParams.get('scenarioId'))
            .subscribe(r => {
                this.scenarioResult = r;
                this.loadingStats = false;
            });
    }

    ngOnDestroy() {
        this.state.publisher.next(
            new RemoveFromBreadcrumbAction('ScenarioResultDetails'));
    }

    public scenarioResult: TestScenarioResult;
    public loadingStats: boolean;
    public appId: String;
}