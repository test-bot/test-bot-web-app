import {TestScenarioResult} from './test-scenario-result';

export class TestResults {
    
    constructor() {
        // code...
    }

    public runId: String;
    public appId: String;
    public started: Date;
    public ended: Date;
    public passed: number;
    public failed: number;
    public machineName: String;
    public scenarios: TestScenarioResult[];
}