import {Change} from './change';

export class Mutation {
    constructor() {
    }

    public mutationId: String;
    public targetSelector: String;
    public mutatedElementSelector: String;
    public type: String;
    public outerHTML: String;
    public report: String;
    public changes: Change[];
}