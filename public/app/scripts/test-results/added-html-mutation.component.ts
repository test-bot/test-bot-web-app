import {Component, Input, OnInit} from '@angular/core';

import {Mutation} from './mutation';
import {TestResultsDataService} from './test-results-data-service';
import {ExecutedStep, Verification} from './executed-step';

@Component({
    selector: 'added-html-mutation',
    templateUrl: 'app/templates/test-results/added-html-mutation.html'
})
export class AddedHtmlMutationComponent implements OnInit {
    @Input() public mutation: Mutation;
    @Input() public executedStep: ExecutedStep;
    @Input() public baseStep: ExecutedStep;
    public formattedReport: String;
    public parsedLines: any[];
    public hasChanges: boolean;
    public issue;

    constructor(private resultsService: TestResultsDataService) {
    }

    ngOnInit() {
        var issues = this.executedStep.issues
            .filter(c => c.id === this.mutation.mutationId || c.executed === this.mutation.mutationId);

        if (issues.length > 0) {
            this.issue = issues[0];
        }

        if (this.mutation.changes && this.mutation.changes.length > 0) {
            this.hasChanges = true;
            this.generateDiffReport();
        }
    }

    approve() {
        var that = this;
        this.resultsService.approveChanges(
            this.executedStep.originalStepId,
            this.executedStep.stepId,
            [this.issue],
            this.executedStep.appId)
        .subscribe(() => {
            that.issue.verification = Verification[Verification.Approved];
        });
    }

    private generateDiffReport() {
        var diffData = {};
        if (!this.mutation.report) return;

        // Restructure the report tags in order to be more easily parsed.
        var report = $(this.mutation.report.toString());
        report.find('span[changeId]').replaceWith(function() {
            var id = $(this).attr('id');
            var changeId = $(this).attr('changeId');
            var prev = $(this).attr('previous');
            var next = $(this).attr('next');
            var changes = $(this).attr('changes');
            var className = $(this).attr('class');

            if (diffData[changeId]) {
                diffData[changeId].total += 1;
            } else {
                diffData[changeId] = {
                    prev: prev,
                    next: next,
                    changes: changes,
                    className: className,
                    total: 1,
                    current: 0
                };
            }

            if(id) {
                return $('<span main-changeId="' + id + '">' + (this.textContent || this.innerHTML) + '</span>');
            }
            return $('<span changeId="' + changeId + '">' + (this.textContent || this.innerHTML) + '</span>');
        });

        var options = {
            source: report[1].outerHTML,
            mode: "beautify", //  beautify, diff, minify, parse
            lang: "html",
            wrap: 6000, // high in order to keep all attributes on one line
            inchar: " ",  // indent character
            insize: 4      // number of indent characters per indent
        }

        var formattedReport: String = prettydiff(options);

        var lines = formattedReport.split(/\r\n|[\n\v\f\r\x85\u2028\u2029]/);
        this.parsedLines = [];
        var wrapperClass; //used to mark each line for added/removed
        for (var i = 0; i < lines.length; i++) {
            var line = lines[i];

            var diffSpan = this.getDiffSpan(line);

            if (diffSpan && !diffSpan[3]) {   // when <span changeId="diff"></span>
                if (diffSpan[1] != 'main-') {
                    wrapperClass = diffData[diffSpan[2]].className;
                }
            }

            if (diffSpan) {
                var encodedLine = this.encodeLine(line);
                this.parsedLines.push({
                    wrapperClass: diffData[diffSpan[2]].className,
                    lineText: encodedLine,
                    changes: diffData[diffSpan[2]].changes
                });

                diffData[diffSpan[2]].current += 1;

                var total = diffData[diffSpan[2]].total;
                var current = diffData[diffSpan[2]].current;
                if (total === current) {
                    wrapperClass = ''
                }
            } else {
                this.parsedLines.push({
                    wrapperClass: wrapperClass,
                    lineText: this.escapeHTML(line)
                });
            }
        }

        setTimeout(() => { $('pre[data-toggle="popover"]').popover({
            trigger: 'hover',
            placement: 'bottom',
            html: true
        })}, 0);
    }

    private getDiffSpan(htmlString: string) {
        return this.spanRegex.exec(htmlString);
    }

    private encodeLine(line: string) {
        var matchedLine = this.wholeLineSpanRegex.exec(line);
        var parts = [
            this.escapeHTML(matchedLine[1]),
            matchedLine[2],
            this.escapeHTML(matchedLine[3]),
            matchedLine[4],
            this.escapeHTML(matchedLine[5]),
        ];

        return parts.join('');
    }

    private escapeHTML(s): String {
        return s.replace(/[&"<>]/g, function(c) {
            return {
                '&': "&amp;",
                '"': "&quot;",
                '<': "&lt;",
                '>': "&gt;"
            }[c];
        });
    }

    private spanRegexStr = "<span ([a-zA-Z]*-)?changeId=['|\"]([a-zA-Z0-9\\-]+)['|\"]>(.*)<\\/span>"
    private spanRegex = new RegExp(this.spanRegexStr, 'i');
    private wholeLineSpanRegex = /(.*)(<span (?:[a-zA-Z]*-)?changeId=['|"][a-zA-Z0-9\-]+['|"]>)(.*)(<\/span>)(.*)/i;
}

declare function prettydiff (params: any) : any;