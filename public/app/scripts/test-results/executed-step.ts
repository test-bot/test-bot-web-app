import {Mutation} from './mutation';

export class ExecutedStep {
    constructor() {

    }

    public scenarioId: String;
    public runId: String;
    public appId: String;
    public stepId: String;
    public status: String;
    public errors: String[];
    public type: String;
    public targetSelector: String;
    public mutations: Mutation[];
    public issues: {
        id: String,
        verification: Verification,
        type: IssueType,
        base: String,
        executed: String
    }[];
    public originalStepId: String;
    public isBaseline: boolean;
}

export enum Verification {
    Approved,
    Bug
}

export enum IssueType {
    Expected,
    Changed
}