import {Injectable, Inject} from '@angular/core';
import {Http, Headers, Response, URLSearchParams} from '@angular/http';
import {Observable} from 'rxjs';
import 'rxjs/add/operator/map';

import {AuthenticationService} from '../authentication-service';
import {TestResults} from './test-results';
import {TestScenarioResult} from './test-scenario-result';
import {ExecutedStep} from './executed-step';

@Injectable()
export class TestResultsDataService {
    constructor(private http: Http, @Inject('appConfig') config, private authService: AuthenticationService) {
        this.endpoint = config.apiServerUrl + '/apps/';
    }

    public get(appId: String) {
        return this.http.get(this.endpoint + appId + '/test-runs', {
            headers: this.authService.getAuthorizationHeader()
        })
            .map<TestResults[]>(res => res.json().items)
            .do(trs => {
                trs.forEach(tr => {
                    if (tr.started)
                        tr.started = new Date(tr.started.toString());

                    if(tr.ended)
                        tr.ended = new Date(tr.ended.toString());
                });
            });
    }

    public getOne(appId: String, testResultsId: String) {
        return this.http.get(this.endpoint + appId + '/test-runs/' + testResultsId, {
            headers: this.authService.getAuthorizationHeader()
        })
            .map<TestResults>(res => res.json())
            .do(tr => {
                if (tr.started)
                    tr.started = new Date(tr.started.toString());

                if (tr.ended)
                    tr.ended = new Date(tr.ended.toString());
            });
    }

    public getOneScenarioResult(appId: String, testResultsId: String, scenarioId: String) {
        return this.http.get(this.endpoint + appId + '/test-runs/' + testResultsId + '/scenario/' + scenarioId, {
            headers: this.authService.getAuthorizationHeader()
        })
            .map<TestScenarioResult>(res => res.json())
            .do(tr => {
                if (tr.started)
                    tr.started = new Date(tr.started.toString());

                if (tr.ended)
                    tr.ended = new Date(tr.ended.toString());

                if(tr.started && tr.ended) {
                    tr.duration = (tr.ended.getTime() - tr.started.getTime()) / 1000;
                }
            });
    }

    public getExecutedSteps(appId: String, testResultsId: String, scenarioId: String) {
        return this.http.get(this.endpoint + appId + '/test-runs/' + testResultsId + '/scenario/' + scenarioId + '/steps', {
            headers: this.authService.getAuthorizationHeader()
        })
            .map<ExecutedStep[]>(res => res.json().items);
    }

    public getExecutedStep(appId: String, testResultsId: String, scenarioId: String, stepId: String) {
        return this.http.get(this.endpoint + appId + '/test-runs/' + testResultsId + '/scenario/' + scenarioId + '/steps/' + stepId, {
            headers: this.authService.getAuthorizationHeader()
        })
            .map<ExecutedStep>(res => res.json());
    }

    public getStepBaseline(appId: String, originalStepId: String) {
        return this.http.get(this.endpoint + appId + '/test-runs/baseline/' + originalStepId, {
            headers: this.authService.getAuthorizationHeader()
        })
            .map<ExecutedStep>(res => res.json());
    }

    public getScenarioHistory(appId: String, scenarioId: String, limit: number) {
        let params: URLSearchParams = new URLSearchParams();
        params.set('take', limit.toString());

        return this.http.get(this.endpoint + appId + '/test-runs/history/' + scenarioId, {
            headers: this.authService.getAuthorizationHeader(),
            search: params
        })
            .map<TestResults[]>(res => res.json().items)
            .do(trs => {
                trs.forEach(tr => {
                    if (tr.started)
                        tr.started = new Date(tr.started.toString());

                    if (tr.ended)
                        tr.ended = new Date(tr.ended.toString());
                });
            });;
    }

    public approveChanges(
        originalStepId: String,
        executedStepId: String,
        issues: any[],
        appId: String) {
            var headers = this.authService.getAuthorizationHeader();
            headers.append('Content-Type', 'application/json');
            return this.http.put(this.endpoint + appId + '/verification/approve', JSON.stringify({
                originalStepId: originalStepId,
                executedStepId: executedStepId,
                issues: issues
            }), {
                headers: headers
            });
    }

    private endpoint;
}