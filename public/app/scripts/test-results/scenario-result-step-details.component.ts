import {Component, OnInit} from '@angular/core';
import {ROUTER_DIRECTIVES, Router, RouteParams} from '@angular/router-deprecated';

import {ExecutedStep, IssueType} from './executed-step';
import {TestResultsDataService} from './test-results-data-service';
import {AddedHtmlMutationComponent} from './added-html-mutation.component';
import {RemovedHtmlMutationComponent} from './removed-html-mutation.component';

@Component({
    templateUrl: 'app/templates/test-results/scenario-result-step-details.html',
    directives: [ROUTER_DIRECTIVES, AddedHtmlMutationComponent, RemovedHtmlMutationComponent]
})
export class ScenarioResultStepDetailsComponent implements OnInit {

    constructor(private routeParams: RouteParams, private router: Router, private testResultsService: TestResultsDataService) {
        // code...
    }

    ngOnInit() {
        this.loading = true;

        var appId = this.router.parent.parent.parent.parent.currentInstruction.component.params['appId'];
        var runId = this.router.parent.parent.currentInstruction.component.params['runId'];
        var scenarioId = this.router.parent.parent.currentInstruction.component.params['scenarioId'];

        this.testResultsService.getExecutedStep(appId, runId, scenarioId, this.routeParams.get('stepId'))
            .subscribe(s => {
                this.step = s;
                this.loading = false;

                var expectedMutations = this.step.issues.filter((i) => {
                    return i.type.toString() === IssueType[IssueType.Expected];
                });
                if(expectedMutations.length > 0) {
                    this.testResultsService.getStepBaseline(appId, this.step.originalStepId)
                        .subscribe((baseStep) => {
                            baseStep.mutations = baseStep.mutations.filter((m)=> {
                                return this.step.issues.filter((em) => {
                                    return em.id === m.mutationId;
                                }).length > 0;
                            });

                            this.baseStep = baseStep;
                        });
                }
            });
    }

    public loading: boolean;
    public step: ExecutedStep;
    public baseStep: ExecutedStep;
}