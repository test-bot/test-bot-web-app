import {Component, OnInit} from '@angular/core';
import {ROUTER_DIRECTIVES, Router} from '@angular/router-deprecated';

import {TestResultsDataService} from './test-results-data-service';
import {TestResults} from './test-results';

@Component({
    templateUrl: 'app/templates/test-results/all-test-results.html',
    directives: ROUTER_DIRECTIVES
})
export class AllTestResultsComponent implements OnInit {

    constructor(private router: Router, private testResultsService: TestResultsDataService) {
    }

    ngOnInit() {
        this.loading = true;
        var appId = this.router.parent.parent.parent.currentInstruction.component.params['appId'];
        this.testResultsService.get(appId)
            .subscribe(r => {
                this.testResults = r;
                this.loading = false;
                setTimeout(() => this.initDataTable(), 0);
            });
    }

    initDataTable() {
        var jTable: any = $('#testResultsTable');
        jTable.DataTable({
            dom: '<"html5buttons"B>lTfgitp',
            paging: false,
            searching: false,
            info: false,
            buttons: [
                { extend: 'copy' },
                { extend: 'csv' },
                { extend: 'excel', title: 'TestResults' },
                { extend: 'pdf', title: 'TestResults' },

                {
                    extend: 'print',
                    customize: function(win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]

        });
    }

    public testResults: TestResults[];
    public loading: boolean;
}