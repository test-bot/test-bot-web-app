export class TestScenarioResult {
    constructor() {}

    public scenarioId: String;
    public title: String;
    public status: String;
    public message: String;
    public started: Date;
    public ended: Date;
    public duration: number;
}