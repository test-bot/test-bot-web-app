export class Change {
    constructor () {
    }

    public id: String;
    public type: String;
    public changes: String;
}