import {Component, OnInit, OnDestroy} from '@angular/core';
import {ROUTER_DIRECTIVES, RouteConfig, Router} from '@angular/router-deprecated';

import {TestResultsDataService} from './test-results-data-service';
import {AllTestResultsComponent} from './all-test-results.component';
import {TestResultsDetailsComponent} from './test-results-details.component';
import {ScenarioResultDetailsComponent} from './scenario-result-details.component';
import * as breadcrumb from '../common/breadcrumb/breadcrumb';

@Component({
    templateUrl: 'app/templates/test-results/test-results.html',
    directives: ROUTER_DIRECTIVES,
    providers: [TestResultsDataService]
})
@RouteConfig([
    { path: '/', name: 'AllResults', component: AllTestResultsComponent, useAsDefault: true },
    { path: '/:runId', name: 'TestResultsDetails', component: TestResultsDetailsComponent },
    { path: '/:runId/scenario/:scenarioId/...', name: 'ScenarioResultDetails', component: ScenarioResultDetailsComponent }
])
export class TestResultsComponent implements OnInit, OnDestroy {
    
    constructor(private state: breadcrumb.BreadcrumbState, private router: Router) {
        // code...
    }

    ngOnInit() {
        var action = new breadcrumb.AppendToBreadcrumbAction();
        action.displayName = 'Test results';
        action.routeName = 'AllResults';
        action.params = {};
        action.router = this.router;
        this.state.publisher.next(action);
    }

    ngOnDestroy() {
        this.state.publisher.next(
            new breadcrumb.RemoveFromBreadcrumbAction('AllResults'));
    }
}