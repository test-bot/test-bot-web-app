import {Component, Input, OnInit} from '@angular/core';

import {Mutation} from './mutation';
import {ExecutedStep, Verification} from './executed-step';
import {TestResultsDataService} from './test-results-data-service';

@Component({
    selector: 'removed-html-mutation',
    templateUrl: 'app/templates/test-results/removed-html-mutation.html'
})
export class RemovedHtmlMutationComponent implements OnInit {
    @Input() public mutation: Mutation;
    @Input() public baseStep: ExecutedStep;
    @Input() public executedStep: ExecutedStep;
    public issue: any;

    constructor(private resultsService: TestResultsDataService) {
    }

    ngOnInit() {
        var expected = this.executedStep.issues.filter((m) => {
            return m.id === this.mutation.mutationId;
        });

        if(expected.length > 0) {
            this.issue = expected[0];
        }
    }

    approve() {
        var that = this;
        this.resultsService.approveChanges(
            this.executedStep.originalStepId,
            this.executedStep.stepId,
            [this.issue],
            this.executedStep.appId)
            .subscribe(() => {
                that.issue.verification = Verification[Verification.Approved];
            });
    }
}