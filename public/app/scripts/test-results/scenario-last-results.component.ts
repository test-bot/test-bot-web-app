import {Component, OnInit, Input} from '@angular/core';
import {ROUTER_DIRECTIVES, Router, RouteParams, RouteConfig} from '@angular/router-deprecated';

import {TestResults} from './test-results';
import {TestScenarioResult} from './test-scenario-result';
import {ExecutedStep} from './executed-step';
import {TestResultsDataService} from './test-results-data-service';
import {ScenarioResultStepsComponent} from './scenario-result-steps.component';
import {ScenarioResultStepDetailsComponent} from './scenario-result-step-details.component';
import {AppendToBreadcrumbAction, RemoveFromBreadcrumbAction, BreadcrumbState} from '../common/breadcrumb/breadcrumb';

@Component({
    selector: 'scenario-last-results',
    templateUrl: 'app/templates/test-results/scenario-last-results.html',
    directives: ROUTER_DIRECTIVES,
    providers: [TestResultsDataService]
})
export class ScenarioLastResultsComponent implements OnInit {
    @Input() public appId: String;
    @Input() public scenarioId: String;
    @Input() public limit: number;

    constructor(private testResultsService: TestResultsDataService) {
        // code...
    }

    ngOnInit() {
        this.loading = true;
        this.testResultsService.getScenarioHistory(this.appId, this.scenarioId, this.limit)
            .subscribe(r => {
                this.testsResults = r.reverse();
                this.loading = false;

                setTimeout(() => {
                    $('[data-toggle="tooltip"]').tooltip()
                }, 0);
            });
    }

    public testsResults: TestResults[];
    public loading: boolean;
}