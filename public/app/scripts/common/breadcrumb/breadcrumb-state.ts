import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Rx';
import {Subject} from 'rxjs/Subject';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/scan'; 
import {AppendToBreadcrumbAction} from './append-to-breadcrumb-action';
import {RemoveFromBreadcrumbAction} from './remove-from-breadcrumb-action';
import {Action} from './action';
import {AppendToBreadcrumbDelayedAction} from './append-to-breadcrumb-delayed-action';

@Injectable()
export class BreadcrumbState {
    constructor () {
        var initialState = [];
        this.publisher = new Subject<Action>();
        this.state = this.bindBreadcrumbState(initialState, this.publisher);
    }

    bindBreadcrumbState(initState: any[], actions: Observable<Action>)
    : Observable<any[]> {
        var state = this.executeAction(initState, actions);

        return this.wrapIntoBehavior(initState, state);
    }

    executeAction(initState: any[], actions: Observable<Action>)
    : Observable<any[]> {
        return actions.scan((state, action) => {
            if (action instanceof AppendToBreadcrumbDelayedAction) {
                return [...state, action];
            } else if (action instanceof AppendToBreadcrumbAction) {
                const newPart = {
                    displayName: action.displayName,
                    routeName: action.routeName,
                    params: action.params,
                    router: action.router
                };
                return [...state, newPart];
            } else if (action instanceof RemoveFromBreadcrumbAction) {
                return state.filter(s => s.routeName !== action.routeName);
            } else {
                return state;
            }
        }, initState);
    }

    wrapIntoBehavior(init, obs) {
        const res = new BehaviorSubject(init);
        obs.subscribe(s => res.next(s));
        return res;
    }

    public publisher :Subject<Action>;
    public state: Observable<any[]>;
}