import {Router} from '@angular/router-deprecated';
import {Action} from './action';

export class AppendToBreadcrumbAction implements Action {
    constructor() {
        // code...
    }

    public displayName: String;
    public routeName: String;
    public params: any;
    public router: Router;
}