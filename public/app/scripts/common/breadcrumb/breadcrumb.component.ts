import {Component, Input, OnInit} from '@angular/core';
import {FORM_DIRECTIVES, NgClass} from '@angular/common';
import {ROUTER_DIRECTIVES, RouteConfig, Router, RouteDefinition} from '@angular/router-deprecated';
import {BreadcrumbState} from './breadcrumb-state';

@Component({
    selector: 'breadcrumb',
    directives: [FORM_DIRECTIVES, ROUTER_DIRECTIVES, NgClass],
    templateUrl: 'app/templates/common/breadcrumb.html'
})
export class BreadcrumbComponent implements OnInit {

    public urls: String[];

    constructor(private state :BreadcrumbState, private router: Router) {
        this.urls = new Array();
    }

    ngOnInit() {
        this.state.state.subscribe(t => {
            this.parts = t;
        });
    }

    navigateTo(part): void {
        part.router.navigate([part.routeName, part.params]);
    }

    public parts: any[];
}