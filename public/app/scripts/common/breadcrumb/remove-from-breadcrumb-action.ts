import {Action} from './action';

export class RemoveFromBreadcrumbAction implements Action {
    constructor(public routeName: String) {}
}