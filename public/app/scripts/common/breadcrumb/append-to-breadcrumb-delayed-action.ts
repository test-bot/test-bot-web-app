import {Observable} from 'rxjs';
import {AppendToBreadcrumbAction} from './append-to-breadcrumb-action';

export class AppendToBreadcrumbDelayedAction extends AppendToBreadcrumbAction {
    
    constructor(public observable: Observable<AppendToBreadcrumbAction>) {
        super();
        observable.subscribe(a => {
            this.displayName = a.displayName;
            this.routeName = a.routeName;
            this.params = a.params;
            this.router = a.router;
        });
    }
}