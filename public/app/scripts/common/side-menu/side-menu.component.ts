import {Component, Input, OnInit} from '@angular/core';
import {ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {Location} from '@angular/common';
import {Application} from '../../applications/application';

@Component({
    templateUrl: 'app/templates/common/side-menu.html',
    selector: 'side-menu',
    directives: [ROUTER_DIRECTIVES]
})
export class SideMenuComponent implements OnInit {
    @Input() public app: Application;

    constructor(public location: Location) {
    }

    ngOnInit() {
        // Initialize slimscroll for right sidebar
        $('.sidebar-container').slimScroll({
            height: '100%',
            railOpacity: 0.4,
            wheelStep: 10
        });

        $('.navbar-minimalize').show();

        // Minimalize menu
        $('.navbar-minimalize').click(function() {
            $("body").toggleClass("mini-navbar");
            SmoothlyMenu();
        });
    }
}