import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'truncateList'})
export class TruncateListPipe implements PipeTransform {
  transform(value: any, length: number): any {
      if(value instanceof Array) {
          if (!length) {
              return value;
          }

          if (value.length <= length) {
              return value;
          } else {
              var result = value.slice(0, length);
              return result;
          }
      } else {
          return value;
      }
  }
}