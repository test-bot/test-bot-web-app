import {Injectable, Inject} from '@angular/core';
import {Http, Headers, Response} from '@angular/http';
import 'rxjs/add/operator/map'

import {AuthenticationService} from '../authentication-service';

@Injectable()
export class ScenariosDataService {
    constructor(private http: Http, @Inject('appConfig') config, private authService: AuthenticationService) {
        this.endpoint = config.apiServerUrl + '/apps/';
    }

    public get(appId) {
        return this.http.get(this.endpoint + appId + '/scenarios', {
                headers: this.authService.getAuthorizationHeader()
            })
            .map(res => res.json().items);
    }

    public getOne(appId, scenarioId) {
        return this.http.get(this.endpoint + appId + '/scenarios/' + scenarioId, {
                headers: this.authService.getAuthorizationHeader()
            })
            .map(res => res.json());
    }

    public generate(appId, suiteId) {
        var headers = this.authService.getAuthorizationHeader();
        headers.append('Content-Type', 'application/json');

        return this.http.post(this.endpoint + appId + '/scenarios/generate', JSON.stringify({suiteId: suiteId}), {
                headers: headers
            })
            .map(res => res.json());
    }

    public updateSessionsData(appId) {
        return this.http.post(this.endpoint + appId + '/sessions/update', '', {
                headers: this.authService.getAuthorizationHeader()
            })
            .map(res => res.json());
    }

    public delete(appId: String, scenarioId: String) {
        var headers = this.authService.getAuthorizationHeader();
        headers.append('Content-Type', 'application/json');

        return this.http.delete(this.endpoint + appId + '/scenarios/' + scenarioId, {
            headers: headers
        });
    }

    public ignore(appId: String, scenarioId: String, ignored: boolean) {
        var headers = this.authService.getAuthorizationHeader();
        headers.append('Content-Type', 'application/json');

        return this.http.put(this.endpoint + appId + '/scenarios/' + scenarioId + '/ignore', JSON.stringify({ignored: ignored}), {
            headers: headers
        });
    }

    private endpoint;
}