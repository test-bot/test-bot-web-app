import {Component, OnInit} from '@angular/core';
import {RouteParams, ROUTER_DIRECTIVES, Router} from '@angular/router-deprecated';
import {ScenariosDataService} from './scenarios-data-service';
import {ScenarioLastResultsComponent} from '../test-results/scenario-last-results.component';
import {TruncateListPipe} from '../common/pipes/truncate-list.pipe';

@Component({
    selector: 'scenarios-list',
    templateUrl: 'app/templates/scenarios/scenarios-list.html',
    directives: [ROUTER_DIRECTIVES, ScenarioLastResultsComponent],
    pipes: [TruncateListPipe]
})
export class ScenariosListComponent implements OnInit {
    constructor(private scenariosService: ScenariosDataService, private params: RouteParams, private router: Router) {
        
    }

    ngOnInit() {
        this.suiteId = this.params.get('suiteId');

        this.loading = true;
        this.scenariosService.get(this.router.parent.parent.parent.currentInstruction.component.params['appId'])
            .subscribe(scenarios => {
                this.scenarios = scenarios;
                this.loading = false;
            });
    }

    public scenarios: any[];
    public loading: boolean;
    public suiteId: String;
}