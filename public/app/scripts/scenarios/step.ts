import {EventType} from './event-type';

export class Step {

    constructor() {
        // code...
    }

    public targetSelector: String;
    public type: EventType;
    public nodeId: String;
}