import {Component, OnInit, OnDestroy} from '@angular/core';
import {Router, RouteParams} from '@angular/router-deprecated';
import {ScenariosDataService} from './scenarios-data-service';
import {Scenario} from './scenario';
import {BreadcrumbState, AppendToBreadcrumbAction, RemoveFromBreadcrumbAction} from '../common/breadcrumb/breadcrumb';
import {ScenarioLastResultsComponent} from '../test-results/scenario-last-results.component';

@Component({
    selector: 'scenario-editor',
    templateUrl: '/app/templates/scenarios/scenario-editor.html',
    directives: [ScenarioLastResultsComponent]
})
export class ScenarioEditorComponent implements OnInit {
    constructor(
        private scenariosService: ScenariosDataService,
        private params: RouteParams,
        private router: Router,
        private state: BreadcrumbState) {

    }

    ngOnInit() {
        var appId = this.router.parent.parent.parent.currentInstruction.component.params['appId'];
        var scenarioId = this.params.get('scenarioId');
        var suiteId = this.params.get('suiteId');
        this.loading = true;
        this.scenariosService.getOne(appId, scenarioId)
            .subscribe(s => {
                this.scenario = s;
                this.loading = false;
                var action = new AppendToBreadcrumbAction();
                action.displayName = this.scenario.name ? this.scenario.name : 'Scenario';
                action.routeName = 'ScenarioDetails';
                action.params = { suiteId: suiteId, scenarioId: scenarioId };
                action.router = this.router;
                this.state.publisher.next(action);
            });
    }

    ngOnDestroy() {
        this.state.publisher.next(
            new RemoveFromBreadcrumbAction(
                'ScenarioDetails'));
    }

    executeStep() {
        frames['yoursite'].contentDocument.querySelector('#sign-up');
    }

    deleteScenario() {
        var appId = this.router.parent.parent.parent.currentInstruction.component.params['appId'];
        swal({
            title: "Delete scenario?",
            text: "The scenario will be deleted.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true
        }, () => {
            this.scenariosService.delete(appId, this.params.get('scenarioId'))
                .subscribe(() => this.router.navigate(['SuiteDetails', { appId: appId, suiteId: this.params.get('suiteId') }]));
        });
    }

    ignoreScenario() {
        var appId = this.router.parent.parent.parent.currentInstruction.component.params['appId'];
        swal({
            title: "Ignore scenario?",
            text: "The scenario won't be executed in the test suites.",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, ignore it!",
            closeOnConfirm: true
        }, () => {
            this.scenariosService.ignore(appId, this.params.get('scenarioId'), true)
                .subscribe(() => this.scenario.ignored = true);
        });
    }

    unignoreScenario() {
        var appId = this.router.parent.parent.parent.currentInstruction.component.params['appId'];
        swal({
            title: "Enable scenario?",
            text: "The scenario will be executed in the test suites.",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, enable it!",
            closeOnConfirm: true
        }, () => {
            this.scenariosService.ignore(appId, this.params.get('scenarioId'), false)
                .subscribe(() => this.scenario.ignored = false);
        });
    }

    public scenario: Scenario;
    public loading: boolean;
}