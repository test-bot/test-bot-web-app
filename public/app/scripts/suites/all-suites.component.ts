import {Component, OnInit} from '@angular/core';
import {ROUTER_DIRECTIVES, Router} from '@angular/router-deprecated';
import {Suite} from './suite';
import {SuitesDataService} from './suites-data-service';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/repeat';

@Component({
    templateUrl: 'app/templates/suites/all-suites.html',
    directives: ROUTER_DIRECTIVES
})
export class AllSuitesComponent implements OnInit {
    constructor(private router: Router, private suiteService: SuitesDataService) {

    }
    ngOnInit() {
        this.loading = true;
        var appId = this.router.parent.parent.parent.currentInstruction.component.params['appId'];

        this.suiteService.get(appId)
            

        this.suites = [];

        this.suiteService.getLastSuitesRuns(appId)

        Observable
            .zip(
                this.suiteService.get(appId).take(1),
                this.suiteService.getLastSuitesRuns(appId).take(1),
                (suites, lastRuns) => {
                    // TODO: refactor when multiple suites are implemented
                    if(suites.length > 0) {
                        let suite: any = suites[0];

                        if(lastRuns.length > 0) {
                            suite.lastRun = lastRuns[0];
                        }

                        return [suite];
                    }
                    
                    return [];
                })
            .subscribe(s => {
                this.suites = s;
                this.loading = false;

                setTimeout(() => {
                    $('[data-toggle="tooltip"]').tooltip()
                }, 0);
            });
    }

    public suites: Suite[];
    public loading: boolean;
}