import {Component, OnInit, OnDestroy} from '@angular/core';
import {RouteParams, ROUTER_DIRECTIVES, RouteConfig, Router} from '@angular/router-deprecated';
import {AllSuitesComponent} from './all-suites.component';
import {SuiteDetailsComponent} from './suite-details.component';
import {SuiteFormComponent} from './suite-form.component';
import {ScenarioEditorComponent} from '../scenarios/scenario-editor.component';
import {SuitesDataService} from './suites-data-service';
import {BreadcrumbState, AppendToBreadcrumbAction, RemoveFromBreadcrumbAction} from '../common/breadcrumb/breadcrumb';
 
@Component({
    templateUrl: 'app/templates/suites/suites.html',
    directives: ROUTER_DIRECTIVES,
    providers: [SuitesDataService]
})
@RouteConfig([
    { path: '/', name: 'AllSuites', component: AllSuitesComponent, useAsDefault: true },
    { path: '/create', name: 'CreateSuite', component: SuiteFormComponent },
    { path: '/:suiteId/edit', name: 'EditSuite', component: SuiteFormComponent },
    { path: '/:suiteId', name: 'SuiteDetails', component: SuiteDetailsComponent },
    { path: '/:suiteId/scenarios/:scenarioId', name: 'ScenarioDetails', component: ScenarioEditorComponent }
])
export class SuitesComponent implements OnInit, OnDestroy {
    
    constructor(private state: BreadcrumbState, private router: Router) {
        // code...
    }

    ngOnInit() {
        var action = new AppendToBreadcrumbAction();
        action.displayName = 'Suites';
        action.routeName = 'AllSuites';
        action.params = {};
        action.router = this.router;
        this.state.publisher.next(action);
    }

    ngOnDestroy() {
        this.state.publisher.next(
            new RemoveFromBreadcrumbAction('AllSuites'));
    }
}