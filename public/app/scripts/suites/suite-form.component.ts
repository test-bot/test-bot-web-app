import {Component, OnInit} from '@angular/core';
import {RouteParams, Router} from '@angular/router-deprecated';
import {SuitesDataService} from './suites-data-service';
import {Suite} from './suite';

@Component({
    templateUrl: 'app/templates/suites/suite-form.html'
})
export class SuiteFormComponent implements OnInit{
    constructor(private params: RouteParams, private router: Router, private suitesService: SuitesDataService) {
        // code...
    }

    ngOnInit() {
        if (this.params.get('suiteId')) {
            this.isEditMode = true;
        } else {
            this.isEditMode = false;
            this.suite = new Suite();
        }
    }

    create() {
        var appId = this.router.parent.parent.parent.currentInstruction.component.params['appId'];
        this.suitesService.create(appId, this.suite)
            .subscribe((result) => {
                this.router.navigate(['SuiteDetails', { suiteId: result.id }]);
            });
    }

    public suite: Suite;
    public isEditMode: boolean;
}