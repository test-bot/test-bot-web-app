import {Injectable, Inject} from '@angular/core';
import {Http, Headers, Response} from '@angular/http';
import {Observable} from 'rxjs';
import 'rxjs/add/operator/map';

import {AuthenticationService} from '../authentication-service';
import {Suite} from './suite';

@Injectable()
export class SuitesDataService {
    constructor(private http: Http, @Inject('appConfig') config, private authService: AuthenticationService) {
        this.endpoint = config.apiServerUrl + '/apps/';
    }

    public get(appId: String) {
        return this.http.get(this.endpoint + appId + '/suites', {
            headers: this.authService.getAuthorizationHeader()
        })
            .map<Suite[]>(res => res.json().items)
            .do(ss => {
                ss.forEach(s => {
                    if (s.lastUpdated)
                        s.lastUpdated = new Date(s.lastUpdated.toString());
                });
            });
    }

    public getOne(appId: String, suiteId: String) {
        return this.http.get(this.endpoint + appId + '/suites/' + suiteId, {
            headers: this.authService.getAuthorizationHeader()
        })
            .map<Suite>(res => res.json())
            .do(s => {
                if (s.lastUpdated)
                    s.lastUpdated = new Date(s.lastUpdated.toString());
            });
    }

    public create(appId: String, suiteParams: Suite) {
        var headers = this.authService.getAuthorizationHeader();
        headers.append('Content-Type', 'application/json');

        return this.http.post(this.endpoint + appId + '/suites', JSON.stringify(suiteParams), {
            headers: headers
        })
            .map<Suite>(res => res.json());
    }

    public delete(appId: String, suiteId: String) {
        var headers = this.authService.getAuthorizationHeader();
        headers.append('Content-Type', 'application/json');

        return this.http.delete(this.endpoint + appId + '/suites/' + suiteId, {
            headers: headers
        });
    }

    public getLastSuitesRuns(appId: String) {
        var headers = this.authService.getAuthorizationHeader();
        headers.append('Content-Type', 'application/json');

        return this.http.get(this.endpoint + appId + '/test-runs/last-runs/suites', {
            headers: headers
        })
            .map(res => res.json().items)
            .do(trs => {
                trs.forEach(tr => {
                    if (tr.started)
                        tr.started = new Date(tr.started.toString());

                    if (tr.ended)
                        tr.ended = new Date(tr.ended.toString());
                });
            });
    }

    private endpoint;
}