import {Component, OnInit, OnDestroy} from '@angular/core';
import {ROUTER_DIRECTIVES, Router, RouteParams} from '@angular/router-deprecated';
import {ScenariosListComponent} from '../scenarios/scenarios-list.component';
import {SuitesDataService} from '../suites/suites-data-service';
import {ScenariosDataService} from '../scenarios/scenarios-data-service';
import {Suite} from './suite';
import {BreadcrumbState, AppendToBreadcrumbAction, RemoveFromBreadcrumbAction, AppendToBreadcrumbDelayedAction} from '../common/breadcrumb/breadcrumb';
import {Subject} from 'rxjs/Subject';
import {Observable} from 'rxjs/Rx';

@Component({
    templateUrl: 'app/templates/suites/suite-details.html',
    directives:[ROUTER_DIRECTIVES, ScenariosListComponent]
})
export class SuiteDetailsComponent implements OnInit, OnDestroy {
    constructor(private params: RouteParams,
        private router: Router,
        private suitesService: SuitesDataService,
        private scenariosService: ScenariosDataService,
        private state: BreadcrumbState) {
        // code...
    }

    ngOnInit() {
        var bcSubject = new Subject<AppendToBreadcrumbAction>();
        this.state.publisher.next(new AppendToBreadcrumbDelayedAction(bcSubject));

        var appId = this.router.parent.parent.parent.currentInstruction.component.params['appId'];

        Observable
            .zip(
                this.suitesService.getOne(appId, this.params.get('suiteId')),
                this.suitesService.getLastSuitesRuns(appId).take(1),
                (suite, lastRuns) => {
                    // TODO: refactor when multiple suites are implemented, add another endpoint - get one stat
                    let s:any = suite;
                        s.lastRun = lastRuns[0];

                        return s;
                    })
            .subscribe(s => {
                this.suite = s;
                var action = new AppendToBreadcrumbAction();
                action.displayName = this.suite.name,
                    action.routeName = 'SuiteDetaiils';
                action.params = { suiteId: this.params.get('suiteId') };
                action.router = this.router;
                bcSubject.next(action);

                setTimeout(() => {
                    $('[data-toggle="tooltip"]').tooltip()
                }, 0);
            });
    }

    ngOnDestroy() {
        this.state.publisher.next(
            new RemoveFromBreadcrumbAction(
                'SuiteDetaiils'));
    }

    deleteSuite() {
        var appId = this.router.parent.parent.parent.currentInstruction.component.params['appId'];
        swal({
            title: "Delete suite?",
            text: "All scenarios and modifications will be deleted.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true
        }, () => {
            this.suitesService.delete(appId, this.params.get('suiteId'))
                .subscribe(() => this.router.navigate(['AllSuites', { appId: appId }]));
        });
    }

    generateScenarios() {
        var appId = this.router.parent.parent.parent.currentInstruction.component.params['appId'];
        this.scenariosService.generate(appId, this.params.get('suiteId'))
            .subscribe(() => {
                alert("Scenarios will be generated soon.");
            });
    }

    public suite: Suite;
}