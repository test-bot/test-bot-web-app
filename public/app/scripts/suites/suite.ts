import {ObjectState} from '../object-state';

export class Suite {

    constructor() {
    }

    public usagePercentage: number;
    public id: String;
    public name: String;
    public state: ObjectState;
    public lastUpdated: Date;
}
