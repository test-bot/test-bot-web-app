import {Component, Inject, Injector, OnInit, provide} from '@angular/core';
import {ResponseOptions} from '@angular/http';
import {RouteConfig, ROUTER_DIRECTIVES} from '@angular/router-deprecated';

import {AppsComponent} from './applications/apps.component';
import {CreateAppComponent} from './applications/create-app.component';
import {AppDetailsComponent} from './applications/app-details.component';
import {AppSettingsComponent} from './applications/app-settings.component';
import {TestDataComponent} from './test-data/test-data.component';
import {BreadcrumbState} from './common/breadcrumb/breadcrumb';

@Component({
    selector: 'my-app',
    templateUrl: 'app/templates/app.component.html',
    directives: [ROUTER_DIRECTIVES],
    providers: [BreadcrumbState]
})
@RouteConfig([
    { path: '/', name: 'AllApps', component: AppsComponent, useAsDefault: true },
    { path: '/add', name: 'AddApp', component: CreateAppComponent },
    { path: '/:appId/...', name: 'AppDetails', component: AppDetailsComponent }
])
export class AppComponent implements OnInit {
    constructor( @Inject('appConfig') private config, private injector: Injector) {

    }

    ngOnInit() {
        if(this.config.accessToken) {
            localStorage.setItem('usabilitics_access_token', this.config.accessToken);
        }
    }
}