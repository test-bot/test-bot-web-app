import {AppComponent}     from './app.component';
import {AuthenticationService} from './authentication-service';
import {TokensDataService} from './applications/tokens-data-service';
import {AppsDataService}  from './applications/apps-data-service'
import {ScenariosDataService}  from './scenarios/scenarios-data-service'
import {bootstrap}        from '@angular/platform-browser-dynamic';
import {ROUTER_PROVIDERS} from '@angular/router-deprecated';
import {LocationStrategy, Location, HashLocationStrategy } from '@angular/common';
import {HTTP_PROVIDERS} from '@angular/http';
import {provide} from '@angular/core';

declare var appConfig: any;

bootstrap(AppComponent, [
    ROUTER_PROVIDERS,
    HTTP_PROVIDERS,
    provide('appConfig', { useValue: appConfig }),
    AppsDataService,
    ScenariosDataService,
    provide(LocationStrategy, { useClass: HashLocationStrategy }),
    AuthenticationService,
    TokensDataService
]);