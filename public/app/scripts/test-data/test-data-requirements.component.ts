import {Component, OnInit} from '@angular/core';
import {RouteParams} from '@angular/router-deprecated';

import {TestDataService} from './test-data-service';
import {TestDataRequirement} from './test-data-requirement';
import {TestDataRequirementComponent} from './test-data-requirement.component';

@Component({
    selector: 'test-data-requirements',
    templateUrl: 'app/templates/test-data/test-data-requirements.html',
    directives: [TestDataRequirementComponent]
})
export class TestDataRequirementsComponent implements OnInit {
    constructor(private testDataServie: TestDataService, private routeParams: RouteParams) {

    }

    ngOnInit() {
        this.loading = true;
        this.testDataServie.getTestDataRequirements(this.routeParams.get("appId"))
            .subscribe(requirements => {
                this.requirements = requirements;
                this.loading = false;
            });
    }

    public requirements: TestDataRequirement[];
    public loading: boolean;
}