import {Component, Input} from '@angular/core';
import {RouteParams} from '@angular/router-deprecated';

import {TestDataRequirement} from './test-data-requirement';
import {TestDataService} from './test-data-service';

@Component({
    selector: 'test-data-requirement',
    templateUrl: 'app/templates/test-data/test-data-requirement.html'
})
export class TestDataRequirementComponent {
    @Input() public requirement: TestDataRequirement;

    constructor(private testDataService: TestDataService, private routeParams: RouteParams) {

    }

    saveTestData() {
        this.loading = true;
        this.testDataService.saveDataRequirement(this.routeParams.get("appId"), this.requirement)
            .subscribe(response => {
                if(response.status === 200) {
                    this.status = "Updated";
                } else {
                    this.status = "Update failed";
                }
                this.loading = false;
            });
    }

    private loading: boolean;
    private status: string;
} 