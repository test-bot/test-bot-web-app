import {Injectable, Inject} from '@angular/core';
import {Http, Headers, Response} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/observable';

import {TestDataRequirement} from './test-data-requirement';
import {AuthenticationService} from '../authentication-service';

@Injectable()
export class TestDataService {
    constructor(private http: Http, @Inject('appConfig') config, private authService: AuthenticationService) {
        this.endpoint = config.apiServerUrl + '/apps/';
    }

    getTestDataRequirements(appId: string) : Observable<TestDataRequirement[]> {
        return this.http.get(this.endpoint + appId + '/test-data/requirements', {
                headers: this.authService.getAuthorizationHeader()
            })
            .map(res => res.json().items);
    }

    saveDataRequirement(appId: string, requirement: TestDataRequirement) {
        var body = {
            testData: requirement.testData,
            requiresData: requirement.requiresData
        };

        var headers = {
            headers: this.authService.getAuthorizationHeader()
        };
        headers.headers.append('Content-Type', 'application/json');

        return this.http.put(this.endpoint + appId  + '/test-data/requirements/' + requirement.nodeId,
            JSON.stringify(body), headers);
    }

    checkForRequirements(appId: string) {
        return this.http.post(this.endpoint + appId + '/test-data/requirements/analyze', '', {
            headers: this.authService.getAuthorizationHeader()
        });
    }

    private endpoint: String;
}