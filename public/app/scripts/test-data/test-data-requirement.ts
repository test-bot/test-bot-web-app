export class TestDataRequirement {
    public nodeId: String;
    public targetSelector: String;
    public targetOuterHTML: String;
    public testData: String;
    public requiresData: boolean;
    public inputType: String;
    public type: String;
}