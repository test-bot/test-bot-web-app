import {Component, OnInit, OnDestroy} from '@angular/core';
import {RouteParams, Router} from '@angular/router-deprecated';

import {TestDataRequirementsComponent} from './test-data-requirements.component';
import {TestDataService} from './test-data-service';
import {BreadcrumbState, AppendToBreadcrumbAction, RemoveFromBreadcrumbAction} from '../common/breadcrumb/breadcrumb';


@Component({
    templateUrl: 'app/templates/test-data/test-data.html',
    directives: [TestDataRequirementsComponent],
    providers: [TestDataService]
})
export class TestDataComponent implements OnInit, OnDestroy {
    constructor(private state: BreadcrumbState, private router: Router, private testDataService: TestDataService, private routeParams: RouteParams) {

    }

    ngOnInit() {
        var action = new AppendToBreadcrumbAction();
        action.displayName = 'Test data';
        action.routeName = 'TestData';
        action.params = {};
        action.router = this.router;
        this.state.publisher.next(action);
    }

    ngOnDestroy() {
        this.state.publisher.next(
            new RemoveFromBreadcrumbAction('TestData'));
    }

    checkForRequirements() {
        this.testDataService.checkForRequirements(this.routeParams.get('appId'))
            .subscribe(response => {
                this.analyzing = true;
            });
    }

    private analyzing: boolean;
}