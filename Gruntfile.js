module.exports = function (grunt) {
    grunt.loadNpmTasks('grunt-ts');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-less');
 
    var appDependencies = [
        "node_modules/core-js/client/shim.min.js",
        "node_modules/zone.js/dist/zone.js",
        "node_modules/reflect-metadata/Reflect.js",
        "node_modules/systemjs/dist/system.src.js",
        "node_modules/rxjs/bundles/Rx.js",
        // "node_modules/@angular/core/core.umd.js",
        // "node_modules/@angular/common/common.umd.js",
        // "node_modules/@angular/compiler/compiler.umd.js",
        // "node_modules/@angular/http/http.umd.js",
        // "node_modules/@angular/router/router.umd.js",
        // "node_modules/@angular/platform-browser-dynamic/platform-browser-dynamic.umd.js",
        "node_modules/jquery/dist/jquery.min.js", //required by the frontend theme
        "node_modules/bootstrap/dist/js/bootstrap.min.js",
        "node_modules/sweetalert/dist/sweetalert.min.js",
        "public/app/lib/prettydiff/dom.js",
        "public/app/lib/prettydiff/language.js",
        "public/app/lib/prettydiff/finalFile.js",
        "public/app/lib/prettydiff/safeSort.js",
        "public/app/lib/prettydiff/csspretty.js",
        "public/app/lib/prettydiff/csvpretty.js",
        "public/app/lib/prettydiff/diffview.js",
        "public/app/lib/prettydiff/jspretty.js",
        "public/app/lib/prettydiff/markuppretty.js",
        "public/app/lib/prettydiff/prettydiff.js"
    ];

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        uglify: {
            options: {
                mangle: false
            },
            default: {
                options: {
                    mangle: false,
                    compress: false,
                    beautify: true,
                    preserveComments: false
                },
                files: [{'public/build/dependencies.js': appDependencies}]
            }
        },

        copy: {
          main: {
            expand: true,
            cwd: 'public/app/',
            src: ['styles/**', 'fonts/**', 'scripts/systemjs.config.js'],
            dest: 'public/build',
          },
        },

        less: {
            default: {
              files: {
                'public/build/styles/style.css': 'public/app/less/style.less'
              }
          }
        },

        ts: {
            default: {
                tsconfig: true,
                options: {
                    "compiler": './node_modules/typescript/bin/tsc',
                }
            }
        },
        watch: {
            files: ['public/app/scripts/**/*.ts', 'public/app/scripts/*.config.js', 'public/app/styles/**', 'public/app/less/**'],
            tasks: ['uglify', 'ts','less', 'copy']
        }
    });
 
    grunt.registerTask('default', ['uglify', 'ts', 'less', 'copy']);
    grunt.registerTask('dev', ['watch']);
}