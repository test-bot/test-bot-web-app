var express = require('express');
var favicon = require('static-favicon');
var logger = require('morgan');
var methodOverride = require('method-override');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var errorHandler = require('errorhandler');
var path = require('path');
var exphbs  = require('express-handlebars');
var uuid = require('node-uuid');
var Q = require('q');
var mongo = require('mongodb');
var ensureLoggedIn = require("connect-ensure-login").ensureLoggedIn;
var passport = require('passport');
var LinkedInStrategy = require('passport-linkedin-oauth2').Strategy;
var jwt = require('jsonwebtoken');

var apiSettings = {};

var app = express();

var port = process.env.WEB_APP_PORT || 3000;
var apiServerUrl = process.env.API_SERVER_URL || 'http://localhost:8081/v1';

// all environments
app.set('port', port);

app.engine('handlebars', exphbs({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');
app.set('views', __dirname + '/views');

app.use(favicon());
app.use(logger('dev'));

// Request body parsing middleware should be above methodOverride
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

app.use(methodOverride());
app.use(cookieParser('something strange'));
app.use(session());
app.use(passport.initialize());
app.use(passport.session());

// Serializes and deserializes the user id into a session cookie.
passport.serializeUser(function(user, done) {
  done(null, user._id);
});

passport.deserializeUser(function(id, done) {
    var collection = apiSettings.database.collection('users');
    collection.find({
        _id: new mongo.ObjectID(id)
    }).limit(1).next(function (err, user) {
        done(err, user);
    });
});

passport.use(new LinkedInStrategy({
    clientID: '7756owx01zxu1b',
    clientSecret: 'dVKO7ZFefGzr0uJG',
    callbackURL: "http://localhost:3000/auth/linkedin/callback",
    scope: ['r_emailaddress', 'r_basicprofile'],
    state: true
  }, linkedinStrategy));

passport.use('linkedin-app', new LinkedInStrategy({
    clientID: '7756owx01zxu1b',
    clientSecret: 'dVKO7ZFefGzr0uJG',
    callbackURL: "http://localhost:3000/auth/app/linkedin/callback",
    scope: ['r_emailaddress', 'r_basicprofile'],
    state: true
  }, linkedinStrategy));

function linkedinStrategy(token, refreshToken, profile, done) {
    var accountId = uuid.v1();

    var collection = apiSettings.database.collection('users');
    collection.findOneAndUpdate({
            linkedin: profile.id
        }, {
            $setOnInsert: { linkedin: profile.id, accountId: accountId}
        }, {
              upsert: true, // insert the document if it does not exist
              returnOriginal: false
          })
        .then(function (result) {
            var token = jwt.sign({
                permissions: ['all'],
                issuedOn: Date.now(),
                type: 'web'
            }, 'YAjGvRT4h58E88zX11PsQAGREm88PUC6', {
                issuer: 'linkedin',
                subject: result.value.accountId,
                expiresIn: 60*60*24 //1day
            });
            result.value.access_token = token;
            return done(null, result.value);
        });
}

// development only
if ('development' == app.get('env')) {
  app.use(errorHandler());
}

app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'node_modules')));

app.get('/auth/app/linkedin', function (req, res, next) {
    var appId = req.query.appId;
    if (appId) {
        req.session.chromeAppId = appId;
    }

    passport.authenticate('linkedin-app')(req, res, next);
});

app.get('/auth/app/linkedin/callback', passport.authenticate('linkedin-app'),
    function (req, res) {
        var callbackUrl = "https://" + req.session.chromeAppId + ".chromiumapp.org/?accessToken=" + req.user.access_token;
        res.redirect(callbackUrl);
});

app.get('/auth/linkedin', passport.authenticate('linkedin'));

app.get('/auth/linkedin/callback', passport.authenticate('linkedin', {
    failureRedirect: '/login'
}), function (req, res) {
    req.session.access_token = req.user.access_token;
    res.redirect("/");
});


app.get('/login', function (req, res) {
    res.render('login.handlebars');
});

app.get('/logout', function (req, res) {
    if(req.user) {
        var collection = apiSettings.database.collection('users');
        collection.updateOne({accountId: req.user.accountId}, {
            $set: {
                lastLogout: Date.now()
        }})
        .then(function () {
            req.logout();

            res.redirect('/');
        });
    } else {
        req.logout();

        res.redirect('/');
    }
});

app.post('/token', ensureLoggedIn('/login'), function (req, res, next) {
    var collection = apiSettings.database.collection('users');
    collection.find({accountId: req.user.accountId})
        .limit(1)
        .next()
        .then(function (user) {
            if (user.apiTokenVersion) {
                user.apiTokenVersion++;
            } else {
                user.apiTokenVersion = 1;
            }
            return user;
        })
        .then(function (user) {
            collection.updateOne({accountId: user.accountId}, {
                $set: {
                    apiTokenVersion: user.apiTokenVersion
                }
            });

            return user;
        })
        .then(function (user) {
            var token = jwt.sign({
                permissions: ['scenarios-read',
                    'scenarios-assert',
                    'test-data',
                    'test-runs-update',
                    'suites-read',
                    'suites-update',
                    'verification-update'],
                type: 'api',
                version: user.apiTokenVersion
            }, 'YAjGvRT4h58E88zX11PsQAGREm88PUC6', {
                subject: req.user.accountId
            });

            res.json({
                accessToken: token
            });
        });
});

app.get('/',ensureLoggedIn('/login'), function(req, res) {
    // Kind of stupid because we rely on the session to pass the token before the login redirect to the redirected request.
    // Still better than passing it as a query param.
    var params = {
        apiServerUrl: apiServerUrl,
        accountId: req.user.accountId
    };
    if (req.session.access_token) {
        params.accessToken = req.session.access_token;
        req.session.access_token = null;
    }
    res.render('index.handlebars', params);
});

/*---- Mongo connection ----*/
var dbUrl = process.env.DBURL || argv.dburl;
if (dbUrl.indexOf('mongodb://') != 0) {
    // add protocol and name of the database
    dbUrl = 'mongodb://' + dbUrl + '/test-bot';
}

var MongoClient = mongo.MongoClient;

// Use connect method to connect to the Server
MongoClient.connect(dbUrl, function(err, db) {
    if (err) {
        console.log("Mongo: failed to connect");
        console.log(JSON.stringify(err, null, 2));
        return;
    }

  console.log("Mongo: Connected to db: " + dbUrl);

  apiSettings.database = db;
});

app.listen(port, function() {
  console.log('Node: listening on port: ' + port);
});